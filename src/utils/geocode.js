const request = require('request');

const geocode = (address) => {
    return new Promise((resolve, reject) => {
        const url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' + encodeURIComponent(address) + '.json?access_token=pk.eyJ1IjoiZGFuaWVhbHZhcmFkb3BlcmVkYSIsImEiOiJjanVkYmV2cGIwN2xvNGVxcnp0YWNhdnFhIn0.5y010Sacma0SPMAyOhEzLA&limit=1';

        request({
            url,
            json: true,
        }, (error, {body}) =>{
            if(error){
                reject('Unable to connect to location service');
            }else if(body.features.length === 0){
                reject('Location not found');
            }else{
                resolve({
                    latitude: body.features[0].center[1],
                    longitude: body.features[0].center[0],
                    location: body.features[0].place_name
                });
            }
        });
    });
}

module.exports = geocode;
