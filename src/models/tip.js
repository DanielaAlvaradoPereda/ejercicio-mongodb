const mongoose = require('mongoose');

const TipSchema = new mongoose.Schema({
    text: String,
    date: Date,
    compliment_count: Number,
    business_id: String,
    user_id: String
});

TipSchema.index({
    text: 'text'
});
const Tip = new mongoose.model('tips', TipSchema);

module.exports = Tip;
