const mongoose = require('mongoose');

const BusinessSchema = new mongoose.Schema({
    business_id: String,
    name: String,
    address: String,
    city: String,
    state: String,
    postal_code: String,
    latitude: Number,
    longitude: Number,
    stars: Number,
    review_count: Number,
    is_open: Number,
    attributes: {
        RestaurantsTakeOut: Boolean,
        BusinessParking: {
            garage: Boolean,
            street: Boolean,
            validated: Boolean,
            lot: Boolean,
            valet: Boolean
        }
    },
    categories: String,
    hours: {
        Monday: [String],
        Tuesday: [String],
        Wednesday: [String],
        Thursday: [String],
        Friday: [String],
        Saturday: [String],
        Sunday: [String]
    },
    location: {
        type: {
            type: String,
            enum: ['Point']
        },
        coordinates: {
            type: [Number]
        }
    }
});

BusinessSchema.statics.restaurants = function(){
    return this.where('categories', /Restaurants/);
};

BusinessSchema.index({
    location: '2dsphere'
})
const Business = new mongoose.model('businesses', BusinessSchema);

module.exports = Business;
