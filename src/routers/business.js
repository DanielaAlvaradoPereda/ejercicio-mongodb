const express = require('express');
const Business = require('../models/business');
const Tip = require('../models/tip')
const geocode = require('../utils/geocode');

const router = new express.Router();

router.get('/count', async (req, res) => {
    if(!req.query.location){
        return res.status(400).send({
            error: 'Location must be provided'
        });
    }

    try{
        const city = new RegExp('^' + req.query.location + '$', 'i');
        const count = await Business.where('city', city).countDocuments();

        res.send({
            count
        });
    }catch(error){
        console.log(error);
        res.status(500).send({
            error: 'Error while retrieving business count'
        });
    }
});

router.get('/restaurant-count', async (req, res) => {
    if(!req.query.location){
        return res.status(400).send({
            error: 'Location must be provided'
        });
    }

    try{
        const city = new RegExp('^' + req.query.location + '$', 'i');
        const restaurantCount = await Business.restaurants().where('city', city).countDocuments();

        res.send({
            restaurant_count: restaurantCount
        })
    }catch(error){
        console.log(error);
        res.status(500).send({
            error: 'Error while retrieving restaurant count'
        });
    }
});

router.get('/closed-restaurant-count', async (req, res) => {
    if(!req.query.location){
        return res.status(400).send({
            error: 'Location must be provided'
        });
    }

    try{
        const city = new RegExp('^' + req.query.location + '$', 'i');
        const closedRestaurantCount = await Business.restaurants().where('city', city)
        .where('is_open', 0).countDocuments();

        res.send({
            closed_restaurant_count: closedRestaurantCount
        });
    }catch(error){
        console.log(error);
        res.status(500).send({
            error: 'Error while retrieving closed count'
        });
    }
});

router.get('/near-restaurants-count', async (req, res) => {
    if(!req.query.location){
        return res.status(400).send({
            error: 'Location must be provided'
        });
    }

    try{
        const {latitude, longitude, location} = await geocode(req.query.location);
        console.log(location);

        const nearRestaurantsCount = await Business.restaurants().find({
            location: {
                $nearSphere: {
                    $geometry: {
                        type: 'Point',
                        coordinates: [
                            longitude,
                            latitude
                        ]
                    },
                    $maxDistance: 2000
                }
            }
        }).count();

        res.send({
            near_restaurants_count: nearRestaurantsCount
        });
    }catch(error){
        console.log(error);
        res.status(500).send({
            error: 'Error while retrieving near restaurants count'
        });
    }
});

router.get('/stars-count', async (req, res) => {
    if(!req.query.location){
        return res.status(400).send({
            error: 'Location must be provided'
        });
    }

    try{
        const city = new RegExp('^' + req.query.location + '$', 'i');
        const starsCount = await Business.aggregate().match({
            city: city,
            categories: /Restaurants/
        }).group({
            _id: {
                $cond: [{$and: [{$gte: ['$stars', 1]}, {$lt: ['$stars', 2]}]}, "[1, 2)", {
                $cond: [{$and: [{$gte: ['$stars', 2]}, {$lt: ['$stars', 3]}]}, "[2, 3)", {
                $cond: [{$and: [{$gte: ['$stars', 3]}, {$lt: ['$stars', 4]}]}, "[3, 4)", {
                $cond: [{$and: [{$gte: ['$stars', 4]}, {$lte: ['$stars', 5]}]}, "[4, 5]", ""]}]}]}]
            },
            count: {
                $sum: 1
            }
        }).project({
            category: '$_id',
            _id: false,
            count: true
        });

        res.send(starsCount);
    }catch(error){
        console.log(error);
        res.status(500).send({
            error: 'Error while retrieving stars count'
        });
    }
});

router.get('/top-5', async (req, res) => {
    if(!req.query.location || !req.query.search || !req.query.day || !req.query.time){
        return res.status(400).send({
            error: 'Location, search, day and time must be provided'
        });
    }

    try{

        const tips = await Tip.find({
            $text: {
                $search: req.query.search
            }
        });
        const business_ids = tips.map((tip) => tip.business_id);

        const {latitude, longitude, location} = await geocode(req.query.location);
        console.log(location);
        const day = req.query.day.charAt(0).toUpperCase() + req.query.day.slice(1);

        const top5 = await Business.restaurants().where('business_id').in(business_ids).find({
            $where: "function(){if(!this.hours || !this.hours." + day + "){return false;}const time = new Date('2019-01-01T" + req.query.time + ":00Z');const openTime = new Date(`2019-01-01T${this.hours." +  day + "[0]}:00Z`);const closeTime = new Date(`2019-01-01T${this.hours." + day + "[1]}:00Z`);return openTime <= time && (closeTime > time || closeTime <= openTime);}",
            location: {
                $nearSphere: {
                    $geometry: {
                        type: 'Point',
                        coordinates: [
                            longitude,
                            latitude
                        ]
                    },
                    $maxDistance: 3000
                }
            }
        }).sort('-stars').limit(5);

        res.send(top5);
    }catch(error){
        console.log(error);
        res.status(500).send({
            error: 'Error while retrieving top 5 businesses'
        });
    }
});

router.get('/food-search', async (req, res) => {
    if(!req.query.location || !req.query.search){
        return res.status(400).send({
            error: 'Location and search must be provided'
        });
    }

    try{
        const tips = await Tip.find({
            $text: {
                $search: req.query.search
            }
        });
        const business_ids = tips.map((tip) => tip.business_id);

        const city = new RegExp('^' + req.query.location + '$', 'i');
        const restaurants = await Business.restaurants().where('city', city).where('business_id').in(business_ids).limit(20);

        res.send(restaurants);
    }catch(error){
        console.log(error);
        res.status(500).send({
            error: 'Error while retrieving restaurants'
        });
    }
});

module.exports = router;
