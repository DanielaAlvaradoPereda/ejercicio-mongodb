const express =  require('express');
require('./db/mongoose');
const businessRouter = require('./routers/business');

const app = express();
const port = process.env.PORT || 3000;

app.use(express.json());
app.use(businessRouter);

app.listen(port, () => {
    console.log('Server is up on port ' + port);
});
