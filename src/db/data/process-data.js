const fs = require('fs');
const readline = require('readline');

// let lines = 0;

const rl = readline.createInterface({
    input: fs.createReadStream('./yelp_academic_dataset_business.json'),
    crlfDelay: Infinity
});

fd = fs.openSync('yelp_academic_dataset_business_formatted.json', 'w');

rl.on('line', (line) => {
    let obj = JSON.parse(line);

    obj.location = {
        'type': 'Point',
        'coordinates': [obj.longitude, obj.latitude]
    };

    if(obj.hours){
        Object.keys(obj.hours).forEach((key) => {
            const times = [];
            obj.hours[key].split('-').forEach(time => time.split(':').forEach(number => times.push(number.length == 2 ? number : '0' + number)));

            obj.hours[key] = [
                `${times[0]}:${times[1]}`,
                `${times[2]}:${times[3]}`
            ];
        });
    }

    fs.writeSync(fd, `${JSON.stringify(obj)}\n`);
    // lines++;
    // if(lines === 1000){
    //     process.exit(0);
    // }
});
